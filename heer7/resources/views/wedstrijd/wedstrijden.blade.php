@extends('master')

@section('title')
    {{ $team->naam }} - Wedstrijden
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="verslag-body">

                @if(count(\App\Wedstrijd::all()) == 0)
                    <p align="center">Geen wedstrijden beschikbaar</p>
                @endif


                    <table class="table table-hover">
                @forelse($wedstrijden as $wedstrijd)
                    <div class="ribbon-left">
                        <span class="content-left"><h2 class="small">{{ $wedstrijd->seizoen->seizoen }}</h2></span>
                    </div>


                        <tr>
                            <td class="wedstrijd-naam">{{ $wedstrijd->thuisteam }} - {{ $wedstrijd->uitteam }} @if($wedstrijd->uitslag) <div class="wedstrijd-uitslag"> {{ $wedstrijd->uitslag }} </div> @endif</td>
                            <td ><div class="wedstrijd-datum"> {{ date("j F Y", strtotime($wedstrijd->datum)) }}</div></td>
                            <td>
                                <div class="wedstrijd-buttons">
                                    @if($verslag = \App\Verslag::where('wedstrijd_id', '=', $wedstrijd->id)->first())
                                        <a class="verslag-button" href="wedstrijden/verslag/{{ $wedstrijd->id }}">
                                            <p>Verslag</p>
                                        </a>
                                    @else
                                        <a class="verslag-button-empty disabled">
                                            <p>Verslag </p>
                                        </a>
                                    @endif

                                    @if(\App\Foto::where('wedstrijd_id', '=', $wedstrijd->id)->first())
                                        <a class="foto-button" href="/wedstrijden/fotos/{{ $wedstrijd->id }}">
                                            <p>FOTOS</p>
                                        </a>
                                    @else
                                        <a class="foto-button-empty disabled">
                                            <p>FOTOS </p>
                                        </a>
                                    @endif



                                </div>
                            </td>
                        </tr>

                @empty

                @endforelse
                </table>
            </div>
        </div>
    </div>
    </div>



@stop