@extends('master')

@section('title')
    {{ $verslag->wedstrijd->thuisteam }} - {{ $verslag->wedstrijd->uitteam }} || Verslag
@stop

@section('facebook')
    <meta property="og:url"           content="http://www.your-domain.com/your-page.html" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content={{ $verslag->wedstrijd->thuisteam }} - {{ $verslag->wedstrijd->uitteam }} ( {{ $verslag->wedstrijd->uitslag }} ) />
    <meta property="og:description"   content={{ $verslag->verslag }} />
    <meta property="og:image"         content="/img/wedstrijd" />
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="/wedstrijden">Wedstrijden</a></li>
                <li class="active">{{ $verslag->wedstrijd->thuisteam }} - {{ $verslag->wedstrijd->uitteam }} ( {{ $verslag->wedstrijd->uitslag }} )</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="ribbon-left">
                <span class="content-left"><h2 class="small">{{ $verslag->wedstrijd->thuisteam }} - {{ $verslag->wedstrijd->uitteam }} ( {{ $verslag->wedstrijd->uitslag }} )</h2></span>
            </div>

            <div class="verslag-body">
                <small>{{ date("j F Y", strtotime($verslag->wedstrijd->datum)) }}</small>
                <?php echo $verslag->verslag ?>
            </div>

            <div style="margin-left: 10px !important;" class="fb-like" data-href=" {{ Request::url() }}" data-width="100%" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>

            {{--<div class="fb-comments" data-width="100%" data-href="{{ Request::url() }}" data-numposts="5"></div>--}}


        </div>
    </div>
@stop