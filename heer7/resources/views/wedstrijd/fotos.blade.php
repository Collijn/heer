@extends('master')

@section('title')
     {{ $wedstrijd->thuisteam }} - {{ $wedstrijd->uitteam }} - Fotos
@stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="/wedstrijden">Wedstrijden</a></li>
                <li class="active">{{ $wedstrijd->thuisteam }} - {{ $wedstrijd->uitteam }} ( {{ $wedstrijd->uitslag }} )</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="ribbon-left ">
                <span class="content-left"><h2>{{ $wedstrijd->thuisteam }} - {{ $wedstrijd->uitteam }} ( {{ $wedstrijd->uitslag }} )</h2></span>
            </div>

            <div class="gallery">
                @foreach($fotos as $foto)
                    <a class="fancybox" rel="gallery1" href="{{ asset('img/' . $wedstrijd->id . '/' . $foto->foto) }}">
                        <img style="margin-bottom: 5px" width="200px" height="200px" src="{{ asset('img/' . $wedstrijd->id . '/' . $foto->foto) }}" alt="" />
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@stop