@extends('master')

@section('title')
    {{ $team->naam }}
@stop

@section('content')
    <div class="row">

        <div class="col-xs-8">
            <div class="ribbon-left">
                <span class="content-left"><h2 class="small">Headlines</h2></span>
            </div>

            <div class="flexslider-main">
                <ul class="slides">

                    @forelse($nieuws as $nieuw)
                    <li data-thumb="slide-thumb.jpg">
                        <div class="flexslider-main-text">
                            <h2 class="flexslider-main-h2">{{ $nieuw->titel }}</h2>
                            <p class="flexslider-main-p">{{ substr($nieuw->nieuws,0,300) }}</p>
                            <a href="/nieuws/{{ $nieuw->id }}"><button type="button" class="btn btn-flex-main btn-default">Lees meer</button></a>
                        </div>
                        <img class="slider-img" style="height: 448px !important" width="100%" src="{{ asset('img/nieuws/' . $nieuw->foto) }}" />
                    </li>
                    @empty
                        <li data-thumb="slide-thumb.jpg">
                            <div class="flexslider-main-text">
                                <h2 class="flexslider-main-h2">Geen nieuws beschikbaar</h2>
                                <p class="flexslider-main-p"> Er is op dit moment geen nieuws beschikbaar </p>
                            </div>
                            <img class="slider-img" style="height: 448px !important" width="100%" src="{{ asset('img/noResult.png') }}" />
                        </li>
                    @endforelse

                </ul>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="ribbon-right">
                <span class="content-right"><h2 class="small">SPELER VAN DE MAAND</h2></span>
            </div>

            <img style="height: 448px !important " class="player-img" width="100%" src="/img/spelers/{{ $svdm->foto }}">

        </div>

    </div>

    <div class="row bottom-row">
        <div class="col-xs-4 col-bot">

            <div class="ribbon-left">
                <span class="content-left"><h2 class="small">FOTOS</h2></span>
            </div>

            @forelse($fotos as $foto)
                <a href="/wedstrijden/fotos/{{ $foto->wedstrijd->id }}">
                    <img  style="height: 50% !important;" class="foto-1" width="100%" src="{{ asset('img/' . $foto->wedstrijd->id . '/' . $foto->foto) }}">
                    <div class="foto-naam foto-naam-1">{{ $foto->wedstrijd->thuisteam }} - {{ $foto->wedstrijd->uitteam }}</div>
                </a>
            @empty
                <p style="margin-top: 60px; text-align: center">Geen fotos beschikbaar</p>
            @endforelse

        </div>
        <div class="col-xs-4 verslag-col">

            <div class="ribbon-center">
                <span class="content-center"><h2 class="small">Verslagen</h2></span>
            </div>

            <div class="verslagen-block">

                @forelse($verslagen as $verslag)
                    <div class="verslag">
                        <h3 class="verslagen-titel" style="margin-bottom: 3px !important;">{{ $verslag->wedstrijd->thuisteam }} - {{ $verslag->wedstrijd->uitteam }} ( {{ $verslag->wedstrijd->uitslag }} )</h3>
                        <small>{{ date("j F Y", strtotime($verslag->wedstrijd->datum)) }}</small>
                        <p>{{ substr($verslag->verslag, 0, 120) }}</p>
                        <a href="/wedstrijden/verslag/{{ $verslag->wedstrijd_id }}" ><small class="verslag-read-more">lees meer</small> </a>
                    </div>
                @empty
                    <p style="margin-top: 60px; text-align: center">Geen verslagen beschikbaar</p>
                @endforelse

            </div>

        </div>


        <div class="col-xs-4 col-bot">

            <div class="ribbon-right">
                <span class="content-right"><h2 class="small">VOLGENDE WEDSTRIJD</h2></span>
            </div>

            <p style="margin-top: 60px; text-align: center">Het seizoen is voorbij</p>

        </div>

    </div>
@stop