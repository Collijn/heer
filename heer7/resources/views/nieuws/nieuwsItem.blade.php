@extends('master')

@section('title')
    {{ $nieuws->titel }} || Nieuws
@stop

@section('content')
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/nieuws">Nieuws</a></li>
            <li class="active">{{ $nieuws->titel }}</li>
        </ol>
    </div>
<div class="row">
    <div class="col-xs-12">

        <div class="ribbon-left">
            <span class="content-left"><h2 class="small">{{ $nieuws->titel }}</h2></span>
        </div>


            <div style="margin-top: 30px" class="media">
                    <a class="pull-left fancybox-news" rel="fancybox-news" href="/img/nieuws/{{ $nieuws->foto }}"><img class="media-nieuws media-object " width="300px;" src="/img/nieuws/{{ $nieuws->foto }}" alt="{{ $nieuws->foto }}"></a>

                    <div class="media-body media-nieuws">
<br><br>
                        <small>{{ date("d-m-Y", strtotime($nieuws->created_at)) }}</small> <br /><br />
                        {{ $nieuws->nieuws }}
                        <br />
                        <div style="margin-top: 10px !important;" class="fb-like" data-href="{{ Request::url() }}" data-width="100%" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>

                    </div>
            </div>

    </div>
</div>
@stop