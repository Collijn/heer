@extends('master')

@section('title')
    {{ $team->naam }} || Nieuws
@stop

@section('content')
<div class="nieuws">
    <div class="row">
        <div class="col-xs-12">
    <div class="ribbon-left">
        <span class="content-left"><h2>Nieuws</h2></span>
    </div>
    @forelse($nieuws as $nieuw)



                    <div class="media">
                        <img class="media-object pull-left media-nieuws" style="width: 250px !important; height: 250px !important;" src="/img/nieuws/{{ $nieuw->foto }}" alt="{{ $nieuw->titel }}">

                        <div class="media-body media-verslagen">
                            <a class="nieuws-title" href="/nieuws/{{ $nieuw->id }}"><h3 class="media-heading">{{ $nieuw->titel }}</h3></a>
                            <small style="margin-top: -10px; font-size: 10px">{{ date("j F Y", strtotime($nieuw->created_at)) }}</small>
                            <p style="margin-bottom: 0">{{ substr($nieuw->nieuws,0,470) }}</p>

                            <a class="nieuws-read-more" href="/nieuws/{{ $nieuw->id }}">Lees meer >></a>
                        </div>
                    </div>



    @empty
        </div>
    </div>
            <div class="col-md-12">
                <div style=" font-size: 30px; text-align: center">Geen nieuws beschikbaar</div>
            </div>
    @endforelse
    {!! $nieuws->render() !!}
</div>
@stop