@extends('manager.master')

@section('title')
    Heer 7 - Manager - Nieuws
@stop

@section('content')
<div style="margin-top: 10px" class="row">
    <div class="col-xs-12">
        @if(\Illuminate\Support\Facades\Session::has('newsitem'))
            <div class="alert alert-success" role="alert">{{ \Illuminate\Support\Facades\Session::get('newsitem') }}</div>
        @endif
    </div>
</div>

<div style="margin-top: 10px" class="row">
    <div class="col-xs-12">
        <button type="button" data-toggle="modal" data-target="#nieuwsModal" class="btn btn-success">Nieuwsitem Toevoegen</button>


        <!-- Modal -->
        <div class="modal fade" id="nieuwsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 style="font-family: bebas_neueregular" class="modal-title" id="myModalLabel">Nieuwsitem Toevoegen</h3>
                    </div>

                    <form method="post" action="/manager/nieuws/add" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="row">

                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control" name="titel" placeholder="Titel">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div style="margin-top: 5px" class="col-xs-12">
                                        <textarea style="resize: none;" name="nieuws" class="form-control" rows="15" placeholder="Nieuws"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-6">

                                        <input style="margin-top: 5px" id="new-nieuws-selector"  name="sourceImage" type="file" >


                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                        <input style="display: none" type="submit" value="Upload Image" name="submitBtn">
                                    </div>
                                </div>
                            </div>


                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-default btn-success" value="Nieuwsitem toevoegen">
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="ribbon-left">
            <span class="content-left"><h2 class="small">Nieuwsitems</h2></span>
        </div>

        <div style="margin-top: 40px" class="nieuws">
            @forelse($nieuws as $nieuw)

                <!-- EDIT MODAL -->
                <div class="modal fade" id="editModal-{{ $nieuw->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 style="font-family: bebas_neueregular" class="modal-title" id="myModalLabel">Nieuwsitem Bewerken</h3>
                            </div>

                            <form method="post" action="/manager/nieuws/edit/{{ $nieuw->id }}" enctype="multipart/form-data">
                                <div class="modal-body">
                                    <div class="row">

                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <input type="text" class="form-control" name="titel" placeholder="Titel" value="{{ $nieuw->titel }}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div style="margin-top: 5px" class="col-xs-12">
                                                <textarea style="resize: none;" name="nieuws" class="form-control" rows="15" placeholder="Nieuws" >{{ $nieuw->nieuws }}</textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-6">

                                                <input style="margin-top: 5px" id="new-nieuws-selector"  name="sourceImage" type="file" >


                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                                <input style="display: none" type="submit" value="Upload Image" name="submitBtn">
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="modal-footer">
                                    <a href="/manager/nieuws/delete/{{ $nieuw->id }}"><button type="button" class="btn btn-danger">Delete</button></a>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <input type="submit" class="btn btn-default btn-success" value="Nieuwsitem bewerken">
                                </div>
                            </form>

                        </div>
                    </div>
                </div>


                <div class="media">
                    <img class="media-object pull-left media-nieuws" style="height: 200px !important;" width="200px;" src="/img/nieuws/{{ $nieuw->foto }}" alt="{{ $nieuw->titel }}">

                    <div class="media-body media-verslagen">
                        <h3 class="media-heading">{{ $nieuw->titel }}</h3>
                        <small style="margin-top: -10px; font-size: 10px">{{ date("j F Y", strtotime($nieuw->created_at)) }}</small>
                        <p style="margin-bottom: 0">{{ substr($nieuw->nieuws,0,470) }}</p>

                        <a class="nieuws-read-more" data-toggle="modal" data-target="#editModal-{{$nieuw->id}}" href="">Bewerk</a>
                    </div>
                </div>
            @empty
                <div class="col-md-12">
                    <div style=" font-size: 30px; text-align: center">Geen nieuws beschikbaar</div>
                </div>
            @endforelse
            {!! $nieuws->render() !!}
        </div>

    </div>
</div>

@stop