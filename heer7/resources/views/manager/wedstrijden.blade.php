@extends('manager.master')

@section('title')
    Heer 7 - Manager - Wedstrijden
@stop

@section('content')

<div style="margin-top: 20px" >
    <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-success">Wedstrijd Toevoegen</button>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 style="font-family: bebas_neueregular" class="modal-title" id="myModalLabel">Wedstrijd Toevoegen</h3>
            </div>

            <form method="post" action="/manager">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">

                            <div class="col-xs-5">
                                <input type="text" class="form-control" name="thuisteam" placeholder="Thuisteam">
                            </div>

                            <div class="col-xs-5">
                                <input type="text" class="form-control" name="uitteam" placeholder="Uitteam">
                            </div>

                        </div>
                    </div>

                    <div style="margin-top: 20px;" class="row">

                        <div class="form-group">

                            <div class="col-xs-5 ">
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' name="datum" class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                </div>
                            </div>

                            <div class="col-xs-3 ">
                                <input type="text" class="form-control" name="uitslag" placeholder="Uitslag*">
                            </div>

                        </div>

                    </div>

                    <div style="margin-top: 20px;" class="row">
                        <div class="form-group">
                            <div class="col-xs-5">
                                <select name="seizoen_id" class="form-control">
                                    @forelse(\App\Seizoen::all() as $seizoen)
                                        <option value="{{ $seizoen->id }}">{{ $seizoen->seizoen }}</option>
                                    @empty
                                        <option>Geen seizoen aanwezig</option>
                                    @endforelse
                                </select>
                            </div>
                        </div>
                    </div>


                    <div style="margin-top: 20px;" class="row">
                        <div class="col-xs-3 ">
                            <small style="color: #c0c0c0">* Optioneel</small>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-default btn-success" value="Wedstrijd toevoegen">
                </div>
            </form>

        </div>
    </div>
</div>


@forelse(\App\Seizoen::orderBy('seizoen', 'desc')->get() as $seizoen)

<div class="row">
<div class="col-xs-12">
<div class="verslag-body">

<div class="ribbon-left">
    <span class="content-left"><h2 class="small">{{ $seizoen->seizoen }}</h2></span>
</div>

<table class="table table-hover">

@forelse($wedstrijden as $wedstrijd)
    <tr>
        <td>
            <a href="/manager/wedstrijd/delete/{{ $wedstrijd->id }}"><button style="height: 20px; width: 20px; margin-top: -3px" type="button" class="btn btn-danger"><span style="margin-left: -7px !important; top: -3px !important;" class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></a>
            <button style="height: 20px; width: 20px; margin-top: -3px" data-toggle="modal" data-target="#editModal-{{$wedstrijd->id}}" type="button" class="btn btn-info"><span style="margin-left: -7px !important; top: -3px !important;" class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>
            {{--EDIT MODAL--}}
            <div class="modal fade" id="editModal-{{$wedstrijd->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h3 style="font-family: bebas_neueregular" class="modal-title" id="myModalLabel">Wedstrijd Bewerken</h3>
                        </div>

                        <form method="post" action="/manager/wedstrijd/edit/{{ $wedstrijd->id }}">
                            <div class="modal-body">
                                <div class="row">
                                    <div class="form-group">

                                        <div class="col-xs-5">
                                            <input type="text" class="form-control" name="thuisteam" placeholder="Thuisteam" value="{{ $wedstrijd->thuisteam }}">
                                        </div>

                                        <div class="col-xs-5">
                                            <input type="text" class="form-control" name="uitteam" placeholder="Uitteam" value="{{ $wedstrijd->uitteam }}">
                                        </div>

                                    </div>
                                </div>

                                <div style="margin-top: 20px;" class="row">

                                    <div class="form-group">

                                        <div class="col-xs-5 ">
                                            <div class='input-group date' id='datetimepicker2'>
                                                <input type='text' name="datum" value="{{ date("j F Y", strtotime($wedstrijd->datum)) }}" class="form-control" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                                            </div>
                                        </div>

                                        <div class="col-xs-3 ">
                                            <input type="text" class="form-control" name="uitslag" placeholder="Uitslag*" value="{{ $wedstrijd->uitslag }}">
                                        </div>

                                    </div>

                                </div>

                                <div style="margin-top: 20px;" class="row">
                                    <div class="form-group">
                                        <div class="col-xs-5">
                                            <select name="seizoen_id" class="form-control">
                                                @forelse(\App\Seizoen::orderBy("seizoen", "desc")->get() as $seizoen)
                                                    <option @if($wedstrijd->seizoen->id == $seizoen->id)  @endif value="{{ $seizoen->id }}">{{ $seizoen->seizoen }}</option>
                                                @empty
                                                    <option>Geen seizoen aanwezig</option>
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div style="margin-top: 20px;" class="row">
                                    <div class="col-xs-3 ">
                                        <small style="color: #c0c0c0">* Optioneel</small>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <input type="submit" class="btn btn-default btn-success" value="Wedstrijd Bewerken">
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </td>
        <td class="wedstrijd-naam">{{ $wedstrijd->thuisteam }} - {{ $wedstrijd->uitteam }} @if($wedstrijd->uitslag) <div class="wedstrijd-uitslag"> {{ $wedstrijd->uitslag }} </div> @endif</td>
        <td ><div class="wedstrijd-datum"> {{ date("j F Y", strtotime($wedstrijd->datum)) }}</div></td>
        <td>
            <div class="wedstrijd-buttons">
                @if($wedstrijd->verslag)
                    <a class="verslag-manager-button" data-toggle="modal" data-target="#verslagModal{{ $wedstrijd->id }}" href="">
                        <p>Verslag </p>
                    </a>
                @else
                    <a class="verslag-manager-button-empty" data-toggle="modal" data-target="#verslagModal{{ $wedstrijd->id }}" href="">
                        <p>Verslag </p>
                    </a>
                @endif
                {{--Verslag Modal--}}
                <div class="modal fade" id="verslagModal{{ $wedstrijd->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 style="font-family: bebas_neueregular" class="modal-title" id="myModalLabel">Verslag Toevoegen</h3>
                            </div>

                            <form method="post" action="/manager/verslag/{{ $wedstrijd->id }}">
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="form-group">

                                            <div class="col-xs-12">
                                                <textarea name="verslag"  class="form-control" rows="15" placeholder="Verslag">@if($wedstrijd->verslag) {{ $wedstrijd->verslag->verslag }} @endif</textarea>
                                                <script>tinymce.init({ selector:'textarea', menubar: false});</script>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <input type="submit" class="btn btn-default btn-success" value="Verslag toevoegen">
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                @if($wedstrijd->fotos->count())
                    <a class="foto-manager-button" data-toggle="modal" data-target="#fotoModal{{ $wedstrijd->id }}" href="">
                        <p>FOTOS </p>
                    </a>
                @else
                    <a class="foto-manager-button-empty" data-toggle="modal" data-target="#fotoModal{{ $wedstrijd->id }}" href="">
                        <p>FOTOS </p>
                    </a>
                @endif

                {{--Verslag Modal--}}
                <div class="modal fotoModal fade" id="fotoModal{{ $wedstrijd->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 style="font-family: bebas_neueregular" class="modal-title" id="myModalLabel">Fotos toevoegen/verwijderen</h3>
                            </div>

                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <form method="post" action="/manager/fotos/delete/{{$wedstrijd->id}}">
                                            @if($wedstrijd->fotos->count())
                                                <select multiple="multiple" name="images[]" class="image-picker show-html">

                                                    <option value=""></option>

                                                    @foreach($wedstrijd->fotos as $foto)
                                                        <option data-img-src="{{ asset('img/' . $wedstrijd->id . '/' . $foto->foto) }}" value="{{ $foto->id }}"></option>
                                                    @endforeach

                                                </select>
                                            @else
                                                <p style="text-align: center;">Voeg fotos toe!</p>
                                            @endif

                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input style="position: relative; float: left;" type="submit" class="btn delete-fotos @if(!$wedstrijd->fotos->count()) disabled @endif btn-default btn-danger" value="Verwijder geselecteerde">

                                </form>
                                <form action="/manager/fotos/upload/{{ $wedstrijd->id }}" class="upload" method="post" enctype="multipart/form-data">

                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                    <label class="btn btn-default btn-success" for="my-file-selector-{{ $wedstrijd->id }}">
                                        <input onchange="this.form.submit()" id="my-file-selector-{{ $wedstrijd->id }}" multiple name="sourceImage[]" type="file" style="display:none;">
                                        Upload Foto
                                    </label>

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <input style="display: none" type="submit" value="Upload Image" name="submitBtn">
                                </form>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </td>
    </tr>


@empty
    Geen wedstrijden gevonden
@endforelse

</table>
        </div>
    </div>
</div>
@empty
    Geen seizoen gevonden
@endforelse


@stop