@extends('manager.master')

@section('title')
    Heer 7 - Manager - Speler van de maand
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <table style="margin-top: 20px;" class="table table-striped">
                <thead>
                    <tr>
                        <th>Naam</th>
                        <th>Positie</th>
                        <th>speler van de maand</th>
                    <tr>
                </thead>
                <tbody>
                @forelse($spelers as $speler)
                    <tr>
                    <td>{{ $speler->naam }}</td>
                    <td>{{ $speler->positie }}</td>
                    @if($speler->svdm == true)
                        <td><button type="button" class="btn btn-primary disabled btn-xs">Speler van de maand</button></td>
                    @else
                        <td><a href="/manager/svdm/{{ $speler->id }}"><button type="button" class="btn btn-primary btn-xs">Speler van de maand</button></a></td>
                    @endif
                    </tr>
                @empty
                    <p align="center">Geen spelers gevonden</p>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@stop