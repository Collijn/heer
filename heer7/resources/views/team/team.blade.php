@extends('master')

@section('title')
    Heer 7 - Team
@stop


@section('content')


    <div class="row">
        <div class="col-md-12">

            <div class="ribbon-left ">
                <span class="content-left"><h2>Team</h2></span>
            </div>

            <div class="gallery">
                @foreach($spelers as $speler)
                    <a class="fancybox" rel="gallery1" href="{{ asset('img/spelers/' . $speler->foto) }}">
                        <img style="margin-bottom: 5px" width="300px" height="400px" src="{{ asset('img/spelers/' . $speler->foto) }}" alt="" />
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@stop