
$(function () {
    $('.flexslider-main').flexslider({
        animation: "slide",
        directionNav: false
    });
});

$(function () {
    $(".fancybox").fancybox({
        openEffect	: 'none',
        closeEffect	: 'none'
    });
});

$(function () {
    $(".fancybox-news").fancybox({
        prevEffect: 'none',
        nextEffect: 'none'
    });
});

$(function () {
    $('#datetimepicker1').datetimepicker({
        sideBySide: true,
        locale: 'nl'
    });
});

$(function () {
    $('#datetimepicker2').datetimepicker({
        sideBySide: true,
        locale: 'nl'
    });
});
$(function()
{
    $('a[data-toggle="tab"]').on('shown', function () {
        //save the latest tab; use cookies if you like 'em better:
        localStorage.setItem('lastTab', $(this).attr('href'));
    });

    //go to the latest tab, if it exists:
    var lastTab = localStorage.getItem('lastTab');
    if (lastTab) {
        $('a[href=' + lastTab + ']').tab('show');
    }
    else
    {
        // Set the first tab if cookie do not exist
        $('a[data-toggle="tab"]:first').tab('show');
    }
});
$(".image-picker").imagepicker()

//$(function(){
//    $(".delete-fotos").on("click", function(){
//
//        imgArray = [];
//
//        $(".selected > img").each(function(){
//            imgArray.push($(this).attr("src"));
//        });
//
//
//        $.ajax({
//            url: '/manager/fotos/delete',
//            data: { images: imgArray },
//            type: "get",
//            success: function(response) {
//                console.log(response);
//                $(".selected > img").each(function(){
//                    $(this).parent().remove();
//                });
//
//                $('.alert-success').text('Afbeelding verwijderd!').fadeIn("slow").delay(5000).fadeOut('slow');
//            }
//        });
//
//    })
//})

