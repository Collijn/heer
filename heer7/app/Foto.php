<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
    //
    public $timestamps = false;

    public function Wedstrijd()
    {
        return $this->belongsTo('App\Wedstrijd');
    }
}
