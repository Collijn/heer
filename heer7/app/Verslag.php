<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Verslag extends Model
{
    //
    protected $table = 'verslagen';

    public function Wedstrijd()
    {
        return $this->belongsTo('App\Wedstrijd');
    }
}
