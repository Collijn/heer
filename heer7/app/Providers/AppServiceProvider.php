<?php

namespace App\Providers;

use App\Team;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $team = Team::first();

        if(!$team)
        {
            $team = new Team;
            $team->naam = 'Heer 5';
            $team->adress = "Laan in den Drink 7";
            $team->logo = "logo.png";
            $team->save();
        }

        view()->share('team', $team);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
