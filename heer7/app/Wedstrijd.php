<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wedstrijd extends Model
{
    //
    protected $table = 'wedstrijden';

    public $timestamps = false;


    protected $fillable = array('thuisteam', 'uitteam', 'datum', 'uitslag');

    public function seizoen()
    {
        return $this->belongsTo('App\Seizoen');
    }

    public function Verslag()
    {
        return $this->hasOne('App\Verslag');
    }

    public function Fotos()
    {
        return $this->hasMany('App\Foto');
    }
}
