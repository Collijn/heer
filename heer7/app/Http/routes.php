<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'homeController@getIndex');

// Wedstrijden routes
Route::get('/wedstrijden', 'wedstrijdController@getWedstrijden');           // Wedstrijden
Route::get('/wedstrijden/verslag/{id}', 'wedstrijdController@getVerslag');  // Verslagen
Route::get('/wedstrijden/fotos/{id}', 'wedstrijdController@getFotos');      // Fotos

// Nieuws routes
Route::get('/nieuws', 'nieuwsController@getNieuws');                        // Al het nieuws
Route::get('/nieuws/{id}', 'nieuwsController@showNieuws');                  // Nieuwsitems

// Team routes
Route::get('/team', 'teamController@getTeam');

// Manager routes
Route::group(['prefix' => 'manager'], function () {
    Route::get('/', function() {
        return Redirect('/manager/nieuws');
    });


    // Menu Routes
    Route::get('/wedstrijden', 'managerController@getWedstrijden');
    Route::get('/nieuws', 'managerController@getNieuws');
    Route::get('/svdm', 'managerController@getTeam');

    Route::get('/svdm/{id}', function($id) {
        $speler = \App\Speler::where('svdm', 1)->first();
        $speler->svdm = 0;
        $speler->save();

        $speler = \App\Speler::find($id);
        $speler->svdm = 1;
        $speler->save();

        return Redirect('/manager/svdm');
    });

    // Nieuws
    Route::post('/nieuws/edit/{id}', 'managerController@postNieuwsEdit');
    Route::post('/nieuws/add', 'managerController@postNieuwsAdd');
    Route::get('/nieuws/delete/{id}', function($id){
        $nieuws = \App\Nieuws::find($id);
        $nieuws->delete();

        return Redirect('/manager/nieuws');
    });

    Route::post('/', 'managerController@postWedstrijd');
    Route::post('/verslag/{id}', 'managerController@postVerslag');
    Route::post('/fotos/upload/{id}', 'managerController@postFotoUpload');
    Route::post('/fotos/delete/{id}', 'managerController@postFotoDelete');
    Route::post('/wedstrijd/edit/{id}', 'managerController@postWedstrijdEdit');
    Route::get('/wedstrijd/delete/{id}', 'managerController@getWedstrijdDelete');
});
