<?php

namespace App\Http\Controllers;

use App\Speler;
use Illuminate\Routing\Controller as BaseController;


class teamController extends BaseController {

    public function getTeam()
    {
        $spelers = Speler::orderBy('naam', 'asc')->get();

        return View('team.team', ['spelers' => $spelers]);
    }
}