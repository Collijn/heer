<?php

namespace App\Http\Controllers;

use App\Foto;
use App\Nieuws;
use App\Speler;
use App\Team;
use App\Verslag;
use App\Wedstrijd;
use Illuminate\Routing\Controller as BaseController;


class homeController extends BaseController{

    public function getIndex()
    {
        $wedstrijd = Wedstrijd::orderBy('datum')->first();
        $verslagen = Verslag::orderBy('created_at')->take(2)->get();
        $fotos = Foto::groupBy('wedstrijd_id')->take(2)->get();
        $nieuws = Nieuws::orderBy('created_at')->take(5)->get();
        $svdm = Speler::where('svdm', 1)->first();

        return View('home', ['wedstrijd' => $wedstrijd, 'verslagen' => $verslagen, 'fotos' => $fotos, 'nieuws' => $nieuws, 'svdm' => $svdm]);
    }

}