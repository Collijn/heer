<?php

namespace App\Http\Controllers;

use App\Foto;
use App\Verslag;
use App\Wedstrijd;
use Illuminate\Routing\Controller as BaseController;

class wedstrijdController extends BaseController{

    public function getWedstrijden()
    {
        $wedstrijden = Wedstrijd::orderBy('datum', 'asc')->get();

        return View('wedstrijd/wedstrijden', ['wedstrijden' => $wedstrijden]);
    }

    public function getVerslag($id)
    {
        $verslag = Verslag::where('wedstrijd_id', '=', $id)->first();

        return View('wedstrijd.verslag', ['verslag' => $verslag]);
    }

    public function getFotos($id)
    {
        $fotos = Foto::where('wedstrijd_id', '=', $id)->get();
        $wedstrijd = Wedstrijd::find($id);

        return View('wedstrijd.fotos', ['fotos' => $fotos, 'wedstrijd' => $wedstrijd]);
    }
}