<?php

namespace App\Http\Controllers;

use App\Nieuws;
use Illuminate\Routing\Controller as BaseController;

class nieuwsController extends BaseController{

    public function getNieuws()
    {
        $nieuws = Nieuws::orderBy('created_at', 'desc')->paginate(4);

        return View('nieuws.nieuws', ["nieuws" => $nieuws]);
    }

    public function showNieuws($id)
    {
        $nieuws = Nieuws::find($id);

        return View('nieuws.nieuwsItem', ["nieuws" => $nieuws]);
    }

}