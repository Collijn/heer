<?php

namespace App\Http\Controllers;

use App\Foto;
use App\Nieuws;
use App\Speler;
use App\Verslag;
use Illuminate\Http\Request;
use App\Wedstrijd;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class managerController extends BaseController{

    // WEDSTRIJDEN

    public function getWedstrijden()
    {
        $wedstrijden = Wedstrijd::orderBy('datum', 'asc')->get();

        return View('manager.wedstrijden', ['wedstrijden' => $wedstrijden]);
    }

    public function postWedstrijd(Request $request)
    {

        $wedstrijd = New Wedstrijd();
        $wedstrijd->thuisteam = $request->get('thuisteam');
        $wedstrijd->uitteam = $request->get('uitteam');
        $wedstrijd->datum = date("Y-m-d", strtotime($request->get('datum')));
        $wedstrijd->uitslag = $request->get('uitslag');
        $wedstrijd->seizoen_id = $request->get('seizoen_id');
        $wedstrijd->save();

        Return Redirect::to('/manager/wedstrijden');
    }

    public function postWedstrijdEdit($id, Request $request)
    {
        $wedstrijd = Wedstrijd::where('id', '=', $id)->first();
        $wedstrijd->thuisteam = $request->get('thuisteam');
        $wedstrijd->uitteam = $request->get('uitteam');
        $wedstrijd->datum = $request->get('datum');
        $wedstrijd->uitslag = $request->get('uitslag');
        $wedstrijd->seizoen_id = $request->get('seizoen_id');
        $wedstrijd->save();

        Return Redirect::to('/manager/wedstrijden');
    }

    public function getWedstrijdDelete($id){

        $wedstrijd = \App\Wedstrijd::find($id);
        $wedstrijd->delete();

        $verslag = Verslag::where('wedstrijd_id', $id)->delete();
        $fotos = Foto::where('wedstrijd_id', $id)->delete();

        Return Redirect::to('/manager/wedstrijden');
    }

    // VERSLAGEN

    public function postVerslag($id, Request $request)
    {
        $v = $request->get('verslag');


        if(strlen(trim($v)) > 0) {
            if ($verslag = Verslag::where('wedstrijd_id', '=', $id)->first()) {
                $verslag->wedstrijd_id = $id;
                $verslag->verslag = $request->get('verslag');
                $verslag->save();
            } else {
                $verslag = new Verslag();
                $verslag->wedstrijd_id = $id;
                $verslag->verslag = $request->get('verslag');
                $verslag->save();
            }
        } else {
            $verslag = Verslag::where('wedstrijd_id', $id)->delete();
        }

        Return Redirect::to('/manager/wedstrijden');
    }

    // NIEUWS

    public function getNieuws()
    {
        $nieuws = Nieuws::orderBy('created_at', 'desc')->paginate(5);

        return View('manager.nieuws', ['nieuws' => $nieuws]);
    }

    public function postNieuwsAdd(Request $request)
    {
        $file = $request->file('sourceImage');
        $destinationPath = 'img/nieuws/';
        $filename = $file->getClientOriginalName();
        $file->move($destinationPath, $filename);

        $nieuws = new Nieuws();
        $nieuws->titel = $request->get('titel');
        $nieuws->nieuws = $request->get('nieuws');
        $nieuws->foto = $filename;
        $nieuws->save();

        Session::flash('newsitem', 'Het nieuwsitem is toegevoegd!');

        Return Redirect::to('/manager/nieuws');
    }

    public function postNieuwsEdit($id, Request $request)
    {
        $nieuws = Nieuws::find($id);
        $nieuws->titel = $request->titel;
        $nieuws->nieuws = $request->nieuws;

        if($request->file('sourceImage')){
            $file = $request->file('sourceImage');
            $destinationPath = 'img/nieuws/';
            $filename = $file->getClientOriginalName();
            $file->move($destinationPath, $filename);

            $nieuws->foto = $filename;
        }

        $nieuws->save();

        Session::flash('newsitem', 'Het nieuwsitem is bewerkt!');

        Return Redirect::to('/manager/nieuws');
    }
    public function postFotoDelete($id, Request $request)
    {
        foreach($request->get('images') as $foto){
            $foto = \App\Foto::find($foto);
            $foto->delete();
        }

        Return Redirect::to('/manager/wedstrijden');
    }

    // SPELER VAN DE MAAND
    public function getTeam()
    {
        $spelers = Speler::all();

        return View('manager.svdm', ["spelers" => $spelers]);
    }

    // FOTOS
    public function postFotoUpload($id, Request $request)
    {
        foreach($request->file('sourceImage') as $foto)
        {
            $file = $foto;
            $destinationPath = 'img/' . $id . '/';
            $filename = $file->getClientOriginalName();
            $file->move($destinationPath, $filename);

            $foto = new Foto();
            $foto->wedstrijd_id = $id;
            $foto->foto = $filename;
            $foto->save();
        }

        Return Redirect::to('/manager/wedstrijden');
    }

}