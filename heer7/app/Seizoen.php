<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seizoen extends Model
{
    //
    protected $table = 'seizoenen';

    public $timestamps = false;

    public function wedstrijd()
    {
        return $this->hasMany('App\Wedstrijd');
    }
}
