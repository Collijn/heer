<?php

use Illuminate\Database\Seeder;

class SeizoenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('seizoenen')->insert([
            'seizoen' => '2014/2015'
        ]);

        DB::table('seizoenen')->insert([
            'seizoen' => '2015/2016'
        ]);
    }
}
